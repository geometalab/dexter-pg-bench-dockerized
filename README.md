# dexter-pg-bench-dockerized

dockerized https://github.com/gregrahn/join-order-benchmark and https://github.com/ankane/dexter

## Requirements

* docker
* docker-compose

## Usage

```bash
docker-compose build --pull
docker-compose up -d postgres-hypopg
docker-compose run --rm load-benchmark-hypopg
# Wait for very long!
```

Then enter your own dexter commands:
`docker-compose run --rm dexter bash`


Or use the sample example:
`docker-compose up dexter`.

### Run queries using psql

Connect to the postgres instance inside a container which has psql installed, ie:

```bash
docker-compose run --rm postgres-hypopg bash -c "psql postgres://postgres:postgres@postgres-hypopg/postgres"
```
### Run queries using python

As a very basic starting point, see the `python-example` folder.

This should better be expanded and then be used with sqlalchemy or similar,
instead of using plain `psql` with subprocess.

```bash
docker-compose run --rm python-example
```

### cleanup

Using `ctrl-c` to stop dexter, if started using `docker-compose up dexter`,
then `docker-compose stop -t 0`.

### purge all data

If for some reason it is necessary to start over
(**WARNING**: This erases the database, 
will be needed to import again):

```bash
docker-compose down -v
```

import os
import subprocess
from os import listdir
from os.path import isfile, join


DB_CONNECTION = os.environ['PG_CONNECTION']


def run_sql(path_to_sql_script):
    subprocess.run(f"psql {DB_CONNECTION} -f {path_to_sql_script}", shell=True)


if __name__ == '__main__':
    sql_files_folder = './join-order-benchmark'
    sql_files = [join(sql_files_folder, f) for f in listdir(sql_files_folder) if isfile(join(sql_files_folder, f)) and f.endswith('.sql')]
    for file_name in sql_files[0:2]:
        run_sql(file_name)
